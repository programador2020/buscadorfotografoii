package buscadorfotografo;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsultaUsuarios {
    public static void main(String[] args) {
        System.out.println("Hola");
        //1. COnectarnos a la base ded datos
        String usuario = "cine";
        String clave = "cine";
        String url = "jdbc:mariadb://localhost:3306/fotografos";
        String consultaSql = "SELECT * FROM `usuarios`";
        Connection miConexion = null;
        try {
             miConexion = DriverManager.getConnection(url, usuario, clave);
            PreparedStatement miPreparativoSql = miConexion.prepareStatement(consultaSql);
            ResultSet miResultado = miPreparativoSql.executeQuery();
            while (miResultado.next()) {                
                System.out.println("Nombre: " + miResultado.getString("nombre"));
            }
            
        } catch (SQLException miExcepcion) {
            System.out.println("Alerta");
            System.out.println(miExcepcion);
        }finally{
            try {
                if (miConexion != null) {
                    miConexion.close();
                    System.out.println("Cerraste la conexion exitosamente :)");
                   
                }
                
            } catch (SQLException miExepcion2) {
                System.out.println("Error al cerrar la conexion de base de datos");
                System.out.println(miExepcion2);
            }
        }
        System.out.println("Chau");
    }
}
