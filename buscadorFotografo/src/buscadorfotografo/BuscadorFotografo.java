package buscadorfotografo;
public class BuscadorFotografo {
    public static void main(String[] args) {
        System.out.println("[...]");
        
        //Primer usuario INSTANCIADO (creado)
        Usuarios miUsuario1;
        miUsuario1 = new Usuarios();
        miUsuario1.apellido = "Perez";
        miUsuario1.nombre = "Juan";
        miUsuario1.clave = "1234";
        
        System.out.println(miUsuario1.apellido + " " + miUsuario1.nombre + " " + miUsuario1.clave);
        
        //Segundo usuario INSTANCIADO (creado)
        Usuarios miUsuario2;
        miUsuario2 = new Usuarios();
        miUsuario2.apellido = "Lopez";
        miUsuario2.nombre = "Laura";
        miUsuario2.descripcionDelServicio = "Fotografia de comida";
        miUsuario2.email = "laura@lopez.com";
        miUsuario2.tel = 12345678;
        miUsuario2.usuario = "usuarioLaura";
        miUsuario2.clave = "password";
        
        miUsuario2.login();
        
        System.out.println(miUsuario2.nombre + " " + miUsuario2.apellido + " " + miUsuario2.descripcionDelServicio + " " + miUsuario2.email + " " + miUsuario2.usuario + " " + miUsuario2.clave  );
        
        System.out.println("[OK]");
    }
    
}
